import { controls } from '../../constants/controls';

const MIN_NUMBER = 1;
const MAX_NUMBER = 2;
const MAX_HEALTH = 100;
const KICK_TIMEOUT = 10000;

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const leftHealthBar = document.getElementById('left-fighter-indicator');
    const rightHealthBar = document.getElementById('right-fighter-indicator');

    let leftHealthBarValue = MAX_HEALTH;
    let rightHealthBarValue = MAX_HEALTH;

    let leftWeightFactor = MAX_HEALTH / firstFighter.health;
    let rightWeightFactor = MAX_HEALTH / secondFighter.health;

    let isSuperHitKicked = false;

    let isKeyQPressed, isKeyWPressed, isKeyEPressed,
      isKeyUPressed, isKeyIPressed, isKeyOPressed,
      isKeyPlayerOneBlockPressed, isKeyPlayerTwoBlockPressed,
      isKeyPlayerOneAttackPressed, isKeyPlayerTwoAttackPressed = false;

    const [q, w, e] = controls.PlayerOneCriticalHitCombination;
    const [u, i, o] = controls.PlayerTwoCriticalHitCombination;

    let handleActions = (event) => {
      let leftDamage, rightDamage = 0;

      if (event.code === q) isKeyQPressed = true;
      if (event.code === w) isKeyWPressed = true;
      if (event.code === e) isKeyEPressed = true;

      if (event.code === u) isKeyUPressed = true;
      if (event.code === i) isKeyIPressed = true;
      if (event.code === o) isKeyOPressed = true;

      if (event.code === controls.PlayerOneAttack) isKeyPlayerOneAttackPressed = true;
      if (event.code === controls.PlayerTwoAttack) isKeyPlayerTwoAttackPressed = true;
      if (event.code === controls.PlayerOneBlock) isKeyPlayerOneBlockPressed = true;
      if (event.code === controls.PlayerTwoBlock) isKeyPlayerTwoBlockPressed = true;

      if (isPlayerOneBlockPlayerOneAttack() || isPlayerTwoBlockPlayerOneAttack()) {
        rightHealthBar.setAttribute('style', `width: ${rightHealthBarValue}%`);
      } else if (isPlayerTwoBlockPlayerTwoAttack() || isPlayerOneBlockPlayerTwoAttack()) {
        leftHealthBar.setAttribute('style', `width: ${leftHealthBarValue}%`);
      } else if (isKeyPlayerOneAttackPressed) { //P1 attack
        rightDamage = getDamage(firstFighter, secondFighter) * rightWeightFactor;
        rightHealthBarValue -= rightDamage;
        showDamage(rightDamage, firstFighter, rightHealthBarValue, rightHealthBar);
      } else if (isKeyPlayerTwoAttackPressed) { //P2 attack
        leftDamage = getDamage(secondFighter, firstFighter) * leftWeightFactor;
        leftHealthBarValue -= leftDamage;
        showDamage(leftDamage, secondFighter, leftHealthBarValue, leftHealthBar);
      }

      if (isPlayerOneSuperKick()) { //P1 SUPER KICK
        superHitPromise().then(() => {
          rightDamage = getHitPower(firstFighter) * 2;
          let health = rightHealthBarValue;
          rightHealthBarValue = getHealthBarValue(health, rightDamage, firstFighter);
          rightHealthBar.setAttribute('style', `width: ${rightHealthBarValue}%`);
        });
      }

      if (isPlayerTwoSuperKick()) { //P2 SUPER KICK
        superHitPromise().then(() => {
          leftDamage = getHitPower(secondFighter) * 2;
          let health = leftHealthBarValue;
          leftHealthBarValue = getHealthBarValue(health, leftDamage, secondFighter);
          leftHealthBar.setAttribute('style', `width: ${leftHealthBarValue}%`);
        });
      }

      function getHealthBarValue(healthBarValue, damage, fighter) {
        let health = healthBarValue - damage;
        if (health <= 0) {
          health = 0;
          resolve(fighter);
        }
        return health;
      }

      function isPlayerOneSuperKick() {
        return !isKeyPlayerOneBlockPressed && isKeyQPressed && isKeyWPressed && isKeyEPressed;
      }

      function isPlayerTwoSuperKick() {
        return !isKeyPlayerTwoBlockPressed && isKeyUPressed && isKeyIPressed && isKeyOPressed;
      }

      function isPlayerOneBlockPlayerOneAttack() {
        return isKeyPlayerOneAttackPressed && isKeyPlayerOneBlockPressed;
      }

      function isPlayerTwoBlockPlayerOneAttack() {
        return isKeyPlayerTwoBlockPressed || (isKeyPlayerTwoBlockPressed && isKeyPlayerOneAttackPressed);
      }

      function isPlayerTwoBlockPlayerTwoAttack() {
        return isKeyPlayerTwoAttackPressed && isKeyPlayerTwoBlockPressed;
      }

      function isPlayerOneBlockPlayerTwoAttack() {
        return isKeyPlayerOneBlockPressed || (isKeyPlayerOneBlockPressed && isKeyPlayerTwoAttackPressed);
      }

      function superHitPromise() {
        return new Promise(resolve => {
          if (!isSuperHitKicked) {
            isSuperHitKicked = true;
            resolve(isSuperHitKicked);
            setTimeout(() => {
              isSuperHitKicked = false;
            }, KICK_TIMEOUT);
          }
        })
      }

      function showDamage(damage, fighter, healthBarStartValue, healthBar) {
        if (healthBarStartValue <= 0) {
          healthBarStartValue = 0;
          document.removeEventListener('keydown', handleActions, false);
          resolve(fighter);
        }
        healthBar.setAttribute('style', `width: ${healthBarStartValue}%`);
      }
    };

    document.addEventListener('keydown', handleActions, false);

    document.addEventListener('keyup', event => {
      if (event.code === q) isKeyQPressed = false;
      if (event.code === w) isKeyWPressed = false;
      if (event.code === e) isKeyEPressed = false;

      if (event.code === u) isKeyUPressed = false;
      if (event.code === i) isKeyIPressed = false;
      if (event.code === o) isKeyOPressed = false;

      if (event.code === controls.PlayerOneBlock) isKeyPlayerOneBlockPressed = false;
      if (event.code === controls.PlayerTwoAttack) isKeyPlayerTwoAttackPressed = false;
      if (event.code === controls.PlayerTwoBlock) isKeyPlayerTwoBlockPressed = false;
      if (event.code === controls.PlayerOneAttack) isKeyPlayerOneAttackPressed = false;
    });
  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) {
    damage = 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  let criticalHitChance = MAX_NUMBER - MIN_NUMBER + Math.random() * (MAX_NUMBER - MIN_NUMBER);
  let power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  let dodgeChance = MAX_NUMBER - MIN_NUMBER + Math.random() * (MAX_NUMBER - MIN_NUMBER);
  let power = defense * dodgeChance;
  return power;
}

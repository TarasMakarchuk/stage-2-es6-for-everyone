import { showModal } from "./modal";

export function showWinnerModal(fighter) {
	// call showModal function

	const { name } = fighter;
	showModal({title: 'Winner is ', bodyElement: name });
}

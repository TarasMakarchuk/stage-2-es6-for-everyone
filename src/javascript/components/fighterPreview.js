import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (Boolean(fighter)) {
    const {name, health, attack, defense, source} = fighter;

    const fighterName = createElement({
      tagName: 'span',
      className: 'fighter-preview-name',
    });
    fighterName.innerText = name;

    const fighterHealth = createElement({
      tagName: 'span',
      className: 'fighter-preview-health',
    });
    fighterHealth.innerText = `health - ${health}`;

    const fighterAttack = createElement({
      tagName: 'span',
      className: 'fighter-preview-attack',
    });
    fighterAttack.innerText = `attack - ${attack}`;

    const fighterDefense = createElement({
      tagName: 'span',
      className: 'fighter-preview-defense',
    });
    fighterDefense.innerText = `defense - ${defense}`;

    const imageRotateLeftToRightClassName = position === 'right' ? 'fighter-img-preview___left-to-right' : 'fighter-preview___right';
    const attributes = {src: source};
    const fighterImage = createElement({
      tagName: 'img',
      className: `fighter-preview-medium___img ${imageRotateLeftToRightClassName}`,
      attributes,
    })
    fighterElement.append(fighterName, fighterHealth, fighterAttack, fighterDefense, fighterImage);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
